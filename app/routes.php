<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
/**
 * index routes
 */
Route::get('/', 'App\Controllers\IndexController@getIndex');

/**
 * member routes
 */
Route::get('member/{id}', 'App\Controllers\MemberController@getProfile')->where('id', '[1-9][0-9]?');
Route::controller('member', 'App\Controllers\MemberController');

/**
 * community routes
 */
Route::get('community/{id}', 'App\Controllers\CommunityController@getProfile')->where('id', '[1-9][0-9]?');
Route::controller('community', 'App\Controllers\CommunityController');
