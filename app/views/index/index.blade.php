<!doctype html>
<html lang="jp">
<head>
	<meta charset="UTF-8">
	<title>シェアハウスSNS</title>
	<link rel="stylesheet" href="css/main.css"/>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<script src="js/bootstrap.min.js"></script>

</head>
<body>
<div id="global-header">
	<ul>
		<li>
			<img src="img/home.png" alt=""/>
		</li>
		<li>
			<img src="img/new.png" alt=""/>
		</li>
		<li>
			<img src="img/community.png" alt=""/>
		</li>
		<li>
			<img src="img/profile.png" alt=""/>
		</li>
	</ul>
</div>
<div id="main">

</div>
<div id="global-footer">

</div>
</body>
</html>
