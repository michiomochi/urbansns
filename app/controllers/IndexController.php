<?php

namespace App\Controllers;

class IndexController extends BaseController
{
    public function getIndex()
    {
        return \View::make('index.index');
    }
}
